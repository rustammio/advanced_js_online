const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

// const root = document.createElement("div");
// root.setAttribute("id", this.id);
// root.id = "root";
// document.body.prepend(root);
//
// const list = document.createElement("ul");
// root.append(list);
//
// books.forEach((item, index) => {
//     try {
//         if (!item.hasOwnProperty("author")) {
//             throw new Error(`Object #${index + 1} has no author name`);
//         } else if (!item.hasOwnProperty("name")) {
//             throw new Error(`Object #${index + 1} has no book name`);
//         } else if (!item.hasOwnProperty("price")) {
//             throw new Error(`Object #${index + 1} has no price`);
//         }
//     } catch (e) {
//         console.log(e.message);
//     } finally {
//         if (
//             item.hasOwnProperty("author") &&
//             item.hasOwnProperty("name") &&
//             item.hasOwnProperty("price")
//         )
//             list.innerHTML += `<li>${item.author} - "${item.name}", ${item.price} грн.</li>`;
//     }
// });


const div = document.createElement('div');
div.id = 'root';
document.body.prepend(div);
const list = document.createElement('ul');
div.append(list);

books.forEach(item => {
    try {
        if (item.author !== undefined && item.name !== undefined && item.price !== undefined) {
            list.insertAdjacentHTML('afterbegin', `<li class="item">${item.author}: ${item.name} - ${item.price} грн;</li>`);
        } else if (item.author === undefined) {
            throw  new Error(`Object has no author name`);
        } else if (item.name === undefined) {
            throw  new Error(`Object has no book name`);
        } else if( item.price === undefined)  {
            throw  new Error(`Object has no price`);
        }
    } catch (error) {
        console.log(error.message);
    }
})

